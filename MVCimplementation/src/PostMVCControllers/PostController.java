/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PostMVCControllers;

import PostMVCModels.PostModel;
import PostMVCViews.PostView;

/**
 *
 * @author Kirubel
 */
public class PostController {
    
    public void startApplication() {
        
        PostView view = new PostView();
        view.setVisible(true);
    }
    
    public String getMessage() {
        try {
            PostModel model = new PostModel();
            return model.getData();
        } catch (Exception er) {
            return "There was an error.";
        }
    }
    
    public boolean writeMessage(String message) {
        try {
            PostModel model = new PostModel();
            return model.writeData(message);
        } catch (Exception er) {
            return false;
        }
    }
}
