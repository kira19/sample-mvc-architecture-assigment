/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PostMVCModels;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author Kirubel
 */
public class PostModel {
    public String getData() throws FileNotFoundException, IOException {
        
        if(!(new File("B:\\file.txt").isFile())) {
            
            Files.createFile(Paths.get("B:\\file.txt"));
        }
//        else{
//            System.out.println("the file can not be created");
//        }
        
        String data;
       
        try (BufferedReader reader = new BufferedReader(
                new FileReader("B:\\file.txt"))) {
           
            StringBuilder string = new StringBuilder();
            
           
            String line = reader.readLine();
            string.append("<html>");
            
            while(line != null) {
               
                string.append(line);
                string.append("<br />");
                
                line = reader.readLine();
            }
            string.append("</html>");
            data = string.toString();
        } catch (Exception er) {
            
            data = er.getMessage();
        }
        
        return data;
    }
    
    public boolean writeData(String data) throws IOException, FileNotFoundException
    {
        
        try (BufferedWriter writer = new BufferedWriter(
                                        new FileWriter("B:\\file.txt"))) {
            
            writer.write(data);
           
            return true;
        } catch (Exception er) {
            return false;
           
        }
    }
}
